import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.css']
})
export class MyFormComponent implements OnInit {

  profileForm: FormGroup;
  showAddress = false;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.profileForm = this.fb.group({
      gender: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      age: [''],
      height: [''],
    });
  }

  onCheckboxChange(event: boolean) {
    if (event) {


      this.profileForm.addControl('address', this.fb.group({
        zip : new FormControl(),
        city : new FormControl(),
        street : new FormControl(),
      }));

      this.showAddress = true;

    } else {
      this.profileForm.removeControl('address');

      this.showAddress = false;

    }
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.profileForm.value);
  }

}
