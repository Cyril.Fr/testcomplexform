import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';


@NgModule({
  declarations: [],
  imports: [
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  exports: [
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule
  ]
})
export class AppMaterialModule { }
