import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-one',
  templateUrl: './form-one.component.html',
  styleUrls: ['./form-one.component.css']
})
export class FormOneComponent implements OnInit {

  @Input() parentForm: FormGroup;

  genders: string[] = ['Mr', 'Ms'];
  heights: string[] = ['small', 'medium', 'large'];

  @Output() checkboxChange = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  checkCheckBoxValue(event) {
      this.checkboxChange.emit(event.checked);
  }

}
